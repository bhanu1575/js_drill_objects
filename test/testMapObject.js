let testObject = require('../data.js');
const mapObject = require('../mapObject.js');

function callbackFunction(value, key) {
    return value + '(***new mapped content***)';
}

try {
    let mappedObject = mapObject(testObject,callbackFunction);
    console.log(mappedObject);
} catch (error) {
    console.error(error.message);
}