
function keys (object) {
    if(typeof object == 'object') {
        let objKeys = [];
        for(key in object) {
            objKeys.push(key);
        }
        return objKeys;
    } else {
        throw new Error('Argument passed to the function keys is not of Object type')
    }
}

module.exports = keys
