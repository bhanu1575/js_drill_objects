function values (object) {
    if(typeof object == 'object') {
        let objectValues = [];
        for(key in object) {
            objectValues.push(object[key])
        }
        return objectValues;
    } else {
        throw new Error('Argument passed to the function values is not of object type.')
    }
}


module.exports = values;