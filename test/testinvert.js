let testObject = require('../data.js');
const invert = require('../invert.js');

try {
    let invertedObject = invert(testObject);
    console.log(invertedObject);
} catch (error) {
    console.error(error.message);
}