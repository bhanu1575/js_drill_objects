function defaults(object,defaultObj) {
    if(typeof object == 'object') {
        for(key in defaultObj) {
            if(!object.hasOwnProperty(key)) {
                object[key] = defaultObj[key];
            }
        }
        return object;
    } else {
        throw new Error('Argument passed to the function defaults are not of valid type.')
    }
}

module.exports = defaults;