function mapObject (object , callbackFunction ) {
    if(typeof object == 'object') {

        let mapping_object = {};
        
        for( key in object ) {
            mapping_object[key] = callbackFunction(object[key]);
        }
        return mapping_object;
    } else {
        throw new Error('Arguments passed to the function mapObject is not of object type')
    }
}

module.exports = mapObject;