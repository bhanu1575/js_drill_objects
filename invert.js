function invert (object) {
    if(typeof object == 'object') {
        let invertedObject = {};
        for( key in object ) {
            invertedObject[object[key]] = key;
        }
        return invertedObject;
    } else {
        throw new Error('Argument passed to the function invert is not of type object')
    }
}

module.exports = invert;