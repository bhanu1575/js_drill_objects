function pairs (object) {
    if(typeof object == 'object') {
        let pairsOfObject = [];
        for( key in object ) {
            pairsOfObject.push([key,object[key]]);
        }
        return pairsOfObject;
    } else {
        throw new Error('Arguments passed to the function pairs is not of object type')
    }
}

module.exports = pairs;